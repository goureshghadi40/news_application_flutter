import 'package:flutter/material.dart';

class AppColor {
  static const Color primaryColorLight = Colors.white;
  static const Color textPrimaryColorLight = Colors.black;
  static const Color buttonColorLight = Colors.black;
  static const Color iconColorLight = Colors.black;

  static const Color primaryColorDark = Color(0xFF333333);
  static const Color textPrimaryColorDark = Colors.white;
  static const Color buttonColorDark = Colors.black;
  static const Color iconColorDark = Colors.white;

  static Color accentColor = Color(0xFFFF8A65);

  static const Color primaryColor = Colors.black;
  static const Color white = Colors.white;
  static const Color transparent = Colors.transparent;
  static const Color grey = const Color(0xffbababa);
  static const Color lightGrey = const Color(0xfff2f2f2);
  static const Color backgroundColor = const Color(0xff464646);
  static const Color blackTransparent = const Color(0x6f000000);

}
